GPU Marching Cubes implementation using Vulkan

Usage:
./vkMarchingCubes Shape Resolution

Shape 0: Sphere
	  1: Cayley Surface

Resolution Default: 96

Controls:
Mouse wheel:				Adjust isovalue
Mouse wheel + shift:		Fine adjust isovalue
Drag left mouse button:		Rotate object
Drag right mouse button:	Zoom
Esc:						Quit

Multi monitor support:
If the application is started on a device with multiple monitors, a small Window is shown. Drag it to the desired monitor and hit any button. Escape cancels the selection and terminates the program.