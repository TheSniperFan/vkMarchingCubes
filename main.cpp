#include <iostream>

#include "system/coreTypes.h"
#include "system/applicationCore.h"
#include <SDL.h>

using Main::InputOptions;


bool parseArguments(
        int argc,
        char *argv[],
        InputOptions& outOptions);

void printOptions(
        const InputOptions options);

void printHelp();

int main(int argc, char *argv[])
{
    if (argc == 2
        && (strncmp(argv[1], "-h", 3) == 0
        || strncmp(argv[1], "--help", 7) == 0))
    {
        printHelp();
        return 0;
    }

    InputOptions opt;
    bool initSuccess = parseArguments(
                argc,
                argv,
                opt);
    if(!initSuccess)
    {
        return 1;
    }

    printOptions(opt);
    ApplicationCore *app = new ApplicationCore(
                opt,
                initSuccess);
    if(initSuccess)
    {
        while(app->Update());
    }

    int ret;
    SDL_Log("Application completed: %s\n",
            app->GetError(&ret).c_str());
    delete app;
    return ret;
}




bool parseArguments(
        int argc,
        char *argv[],
        InputOptions& outOptions)
{
    if (argc < 2)
    {
        outOptions.mode = 0;
        outOptions.dim[0] = outOptions.dim[1] = outOptions.dim[2] = 96;
        return true;
    }
    else
    {
        outOptions.mode = atoi(argv[1]);
        outOptions.flip = false;
        switch (outOptions.mode)
        {
        case 0:
        case 1:
            if(argc > 4)
            {
                for (int i = 0; i < 3; ++i)
                {
                    const int d = atoi(argv[i + 2]);
                    outOptions.dim[i] = d > 3 ? d : 96;
                }
            }
            else
            {
                outOptions.dim[0] = outOptions.dim[1] = outOptions.dim[2] = 96;
            }
            return true;
        case 2:
            if(argc > 6)
            {
                SDL_RWops *io = SDL_RWFromFile(argv[2], "rb");
                if(!io)
                {
                    SDL_LogCritical(
                                SDL_LOG_CATEGORY_ERROR,
                                "%s",
                                SDL_GetError());
                    return false;
                }
                else
                {
                    size_t size = SDL_RWsize(io);
                    outOptions.filePath = argv[2];
                    SDL_RWclose(io);

                    if(strlen(argv[3]) != 2
                            && strlen(argv[3]) != 3)
                    {
                        SDL_LogCritical(
                                    SDL_LOG_CATEGORY_ERROR,
                                    "Invalid format! (Correct format: "
                                    "f[16,32,64], [u,s][8,16,32,64]");
                        return false;
                    }

                    std::string enc = argv[3];

                    switch(enc.at(0))
                    {
                    case 'f':
                        outOptions.integer = false;
                        break;
                    case 'u':
                        outOptions.sign = false;
                        outOptions.integer = true;
                        break;
                    case 's':
                        outOptions.sign = true;
                        outOptions.integer = true;
                        break;
                    default:
                        SDL_LogCritical(
                                    SDL_LOG_CATEGORY_ERROR,
                                    "Invalid format! (Correct format: "
                                    "f[16,32,64], [u,s][8,16,32,64]");
                        return false;
                    }

                    int width = atoi(enc.substr(1).c_str());
                    switch (width)
                    {
                    case 8:
                        if(!outOptions.integer)
                        {
                            SDL_LogCritical(
                                        SDL_LOG_CATEGORY_ERROR,
                                        "Invalid format! (Correct format: "
                                        "f[16,32,64], [u,s][8,16,32,64]");
                            return false;
                        }
                    case 16:
                    case 32:
                    case 64:
                        outOptions.width = width;
                        break;
                    default:
                        SDL_LogCritical(
                                    SDL_LOG_CATEGORY_ERROR,
                                    "Invalid format! (Correct format: "
                                    "f[16,32,64], [u,s][8,16,32,64]");
                        return false;
                    }

                    for (int i = 0; i < 3; ++i)
                    {
                        int val = atoi(argv[4 + i]);
                        outOptions.dim[i] = (val > 3) ? val : 96;
                    }

                    size_t expectedSize =
                            outOptions.width / 8
                            * outOptions.dim[0]
                            * outOptions.dim[1]
                            * outOptions.dim[2];

                    if(size != expectedSize)
                    {
                        SDL_LogCritical(
                                    SDL_LOG_CATEGORY_ERROR,
                                    "File size mismatch! Expected "
                                    "%lu, but found %lu!",
                                    size,
                                    expectedSize);
                        return false;
                    }
                }
                if (argc > 7)
                {
                    outOptions.flip = strcmp(argv[7], "flip") == 0;
                }
                return true;
            }
        default:
            return false;
        }
    }
}

void printOptions(
        const InputOptions options)
{
    switch(options.mode)
    {
    case 0:
        SDL_LogInfo(
                    SDL_LOG_CATEGORY_APPLICATION,
                    "\nMode: Test sphere \n"
                    "Dimensions: (%i,%i,%i)",
                    options.dim[0],
                    options.dim[1],
                    options.dim[2]);
        return;
    case 1:
        SDL_LogInfo(
                    SDL_LOG_CATEGORY_APPLICATION,
                    "\nMode: Test cayley surface \n"
                    "Dimensions: (%i,%i,%i)",
                    options.dim[0],
                    options.dim[1],
                    options.dim[2]);
        return;
    case 2:
        SDL_LogInfo(
                    SDL_LOG_CATEGORY_APPLICATION,
                    "\nMode: File \n"
                    "File: %s\n"
                    "Format: %i-Bit %s\n"
                    "Dimensions: (%i,%i,%i)\n"
                    "Byte order flipping: %s",
                    options.filePath.c_str(),
                    options.width,
                    options.integer ? options.sign ?
                            "signed integer"
                            : "unsigned integer"
                            : "floating point",
                    options.dim[0],
                    options.dim[1],
                    options.dim[2],
                    options.flip ? "Yes" : "No");
        return;
    }
}

void printHelp()
{
    SDL_Log("vkMarchingCubes\n"
            "---------------------\n\n"
            "Syntax:\n"
            "./vkMarchingCubes MODE OPTIONS\n\n"
            "./vkMarchingCubes (96x96x96 Sphere)\n"
            "./vkMarchingCubes 1 128 96 96 (128x96x96 Cayley surface)\n"
            "./vkMarchingCubes 2 file.raw f32 100 200 150 flip "
            "(read from file, 100x200x150 voxels, flip byte order)\n\n"

            "Mode: 0 (Sphere) (default)\n"
            "Options:\n"
            "Resolution (3 dim):\n"
            "\tMin:\t\t(4,4,4)\n"
            "\tFallback:\t(96,96,96)\n\n"

            "Mode: 1 (Cayley surface)\n"
            "Options:\n"
            "Resolution (3 dim):\n"
            "\tMin:\t\t(4,4,4)\n"
            "\tFallback:\t(96,96,96)\n\n"


            "Mode: 2 (File)\n"
            "Options:\n"
            "File\n"
            "\tPath to file\n"
            "Format\n"
            "\tFloating point:\t\tf16,f32,f64\n"
            "\tUnsigned integer:\tu8,u16,u32,u64\n"
            "\tSigned integer:\t\ts8,s16,s32,s64\n"
            "Resolution (3 dim)\n"
            "\tMin:\t\t(4,4,4)\n"
            "\tFallback:\t(96,96,96)\n"
            "flip\n"
            "\tFlips byte order, if specified");
}
