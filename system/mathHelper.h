#pragma once


/**
 * @brief LerpF linearlly interpolates a float between two values. Does not
 * clamp the Time value.
 * @param From Value at Time = 0.
 * @param To Value at Time = 1.
 * @param Time not normalized time value.
 * @return Interpolated value.
 */
static float LerpF(
        float From,
        float To,
        float Time)
{
    float ret = From + Time * (To - From);
    return ret;
}

/**
 * @brief ClampF clamps a float between a minimum and a maximum.
 * @param Val Value to be clamped.
 * @param Min Lower limit.
 * @param Max Upper limit.
 * @return Clamped value.
 */
static float ClampF(
        float Val,
        float Min,
        float Max)
{
    return (Val < Min) ? Min : (Val > Max) ? Max : Val;
}
