#pragma once

namespace MarchingCubes
{
    /**
      * 12-bit flags that store all possible combinations of whether any given
      * vertex is in- or outside of the surface. If the vertex n-th vertex is
      * inside the surface, the n-th bit is set to 1.
      */
    extern int EdgeTable[256];

    /**
      * Maps a vertex to the indices of the edges that should be used when creating
      * the facet(s).
      * If only edge 3 was inside the surface, the EdgeTable lookup used 8 (2^3).
      * Using 8 to look up the value in this table returns 3, 11, 2, followed by -1.
      * This means that the facet's vertices lie somewhere along the edges 3
      * (vertices 0, 3), 11 (vertices 3, 7) and 2 (vertices 2, 3).
      */
    extern int TriTable[256][16];
}
