#pragma once

#include <vulkan/vulkan.h>
#include <vector>
#include <array>
#include <glm/glm.hpp>
struct SDL_Window;



namespace  Main
{
    /**
     * @brief The InputOptions struct contains all options that were parsed
     * from the command line arguments.
     */
    struct InputOptions
    {
        int mode;
        int dim[3];

        std::string filePath;
        bool sign;
        bool integer;
        char width;
        bool flip;
    };
}

namespace Core
{
    /**
    * @brief The SDLStateInfo struct contains all the data required for SDL.
    */
    struct SDLStateInfo
    {
        SDL_Window *Window;
        int Window_x;
        int Window_y;
        int Window_h;
        int Window_w;
        uint32_t WindowFlags;
        const char *WindowTitle;
    };

    /**
     * @brief The QueueInfo struct contains the family index, priorities and
     * thus the number of requested queues of a given family.
     */
    struct QueueInfo
    {
        uint32_t FamilyIndex;
        std::vector<float> Priorities;
    };

    /**
     * @brief The ScalarStorageBufferObject struct contains the scalars
     * that are used by the marching cubes algorithm.
     */
    struct ScalarStorageBufferObject
    {
        float *Scalars;
    };

    /**
     * @brief The VulkanContext struct contains all the required information
     * for the vulkan rendering and compute engine to function.
     */
    struct VulkanContextData
    {
        VkInstance Instance;
        VkDevice LogicalDevice;
        VkSurfaceKHR Surface;

        VkPhysicalDevice PhysicalDevice;

        uint32_t GraphicsQueueFamilyIndex;
        uint32_t ComputeQueueFamilyIndex;
        std::vector<QueueInfo> QueueCreateInfo;
        VkQueue GraphicsQueue;
        VkQueue ComputeQueue;

        VkSwapchainKHR Swapchain;
        std::vector<VkImage> SwapchainImages;
        uint32_t SwapchainImageIndex;
        VkFormat ImageFormat;
        VkExtent2D ImageExtent;
        std::vector<VkImageView> SwapchainImageViews;

        std::vector<char> VertexShader;
        std::vector<char> FragmentShader;
        VkShaderModule VertexShaderModule;
        VkShaderModule FragmentShaderModule;

        VkPipeline GraphicsPipeline;
        VkDescriptorSetLayout DescriptorSetLayout;
        VkPipelineLayout PipelineLayout;
        VkRenderPass Renderpass;

        std::vector<VkFramebuffer> SwapchainFramebuffers;

        VkCommandPool CommandPool;
        std::vector<VkCommandBuffer> CommandBuffers;

        VkSemaphore ImageAvailableSemaphore;
        VkSemaphore RenderFinishedSemaphore;

        uint32_t VertexLimit;
        VkBuffer VertexBuffer;
        VkDeviceMemory VertexBufferMemory;

        VkBuffer ScalarBuffer;
        VkDeviceMemory ScalarBufferMemory;
        VkBuffer EdgeTableBuffer;
        VkDeviceMemory EdgeTableBufferMemory;
        VkBuffer TriTableBuffer;
        VkDeviceMemory TriTableBufferMemory;

        VkDescriptorPool DescriptorPool;
        VkDescriptorSet DescriptorSet;

        VkImage DepthImage;
        VkDeviceMemory DepthImageMemory;
        VkImageView DepthImageView;


        std::vector<char> ComputeShader;
        VkShaderModule ComputeShaderModule;

        VkPipeline ComputePipeline;
        VkPipelineLayout ComputePipelineLayout;
        VkBuffer ComputeVertexBuffer;

        uint32_t ComputeWorkgroupCount[3];
    };
}

namespace Model
{
    struct Vertex
    {
        glm::vec4 Position;
        glm::vec4 Normal;


        static VkVertexInputBindingDescription BindingDescription()
        {
            VkVertexInputBindingDescription desc =
            {
                0,
                sizeof(Vertex),
                VK_VERTEX_INPUT_RATE_VERTEX
            };

            return desc;
        }

        static
        std::array<VkVertexInputAttributeDescription, 2>
        AttributeDescriptions()
        {
            std::array<VkVertexInputAttributeDescription, 2> attr =
            {
                VkVertexInputAttributeDescription
                {
                    0,
                    0,
                    VK_FORMAT_R32G32B32_SFLOAT,
                    offsetof(Vertex, Position)
                },
                VkVertexInputAttributeDescription
                {
                    1,
                    0,
                    VK_FORMAT_R32G32B32_SFLOAT,
                    offsetof(Vertex, Normal)
                }
            };

            return attr;
        }
    };
}
