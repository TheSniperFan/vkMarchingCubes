#include "applicationCore.h"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>
#include <limits>

#include "sdlUtils.h"
#include "vulkanUtils.h"
#include "mathHelper.h"
#include "globalSettings.h"
#include "tables.h"



ApplicationCore::ApplicationCore(
        const InputOptions Options,
        bool &outSuccess)
{
    Dimensions[0] = Options.dim[0];
    Dimensions[1] = Options.dim[1];
    Dimensions[2] = Options.dim[2];

    ThresholdMin = std::numeric_limits<float>::max();
    ThresholdMax = -ThresholdMin;
    if(!ReadInputData(Options))
    {
        return;
    }

    outSuccess = false;
    SDLManager = new Core::SDLManager(outSuccess, ErrorCode, ErrorString);
    if(!outSuccess)
    {
        VulkanManager = nullptr;
        return;
    }
    VulkanManager = new Core::VulkanManager(
                SDLManager->SDLState(),
                outSuccess,
                ErrorCode,
                ErrorString,
                Dimensions,
                InputData);
    if(!outSuccess)
    {
        return;
    }

    CameraYaw = 0.0f;
    CameraYawSpeed = 1.0f;
    CurrentFOV = TargetFOV = 60.0f;
    CameraPitch = 0.5f;
    CameraPitchSpeed = 0.0f;
    LastUpdateTime = high_resolution_clock::now();
    DeltaTime = 0.0f;
    CurrentThreshold = LerpF(
                ThresholdMin,
                ThresholdMax,
                0.5f);
    LastThreshold = CurrentThreshold + 1.0f;
}

ApplicationCore::~ApplicationCore()
{
    InputData.clear();
    if(VulkanManager)
    {
        delete VulkanManager;
    }
    if(SDLManager)
    {
        delete SDLManager;
    }
}

bool ApplicationCore::Update()
{
    bool repeat = true;
    UpdateDeltaTime();
    SDLManager->Update();
    const Core::SDLInput input = SDLManager->CurrentInput();
    if(SDLManager->ShouldQuit() || input.Escape)
    {
        repeat = false;
    }

    UpdateCamera();
    GenerateVertexBuffer();
    UpdateMVP();

    vkQueueWaitIdle(VulkanManager->VulkanContext().GraphicsQueue);
    repeat &= RenderFrame();
    vkDeviceWaitIdle(
                VulkanManager->VulkanContext().LogicalDevice);
    return repeat;
}

std::string ApplicationCore::GetError(
        int *OutErrorCode)
{
    if(OutErrorCode)
    {
        *OutErrorCode = ErrorCode;
    }
    return ErrorString;
}

void ApplicationCore::SetError(
        int &Code,
        std::string &Description)
{
    ErrorCode = Code;
    ErrorString = Description;
}

bool ApplicationCore::RenderFrame()
{
    Core::VulkanContextData &vulkanContext =
            VulkanManager->VulkanContext();
    uint32_t imageIndex;

    VkResult res = vkAcquireNextImageKHR(
                vulkanContext.LogicalDevice,
                vulkanContext.Swapchain,
                2000000000,
                vulkanContext.ImageAvailableSemaphore,
                VK_NULL_HANDLE,
                &imageIndex);

    if(res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR)
    {
        SDL_SetError("Couldn't acquire swapchain image (%i)", res);
        return false;
    }

    VkPipelineStageFlags waitStages[] =
    {
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
    };

    VkSubmitInfo submitInfo =
    {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        nullptr,
        1,
        &vulkanContext.ImageAvailableSemaphore,
        waitStages,
        1,
        &vulkanContext.CommandBuffers[imageIndex],
        1,
        &vulkanContext.RenderFinishedSemaphore
    };

    res = vkQueueSubmit(
                vulkanContext.GraphicsQueue,
                1,
                &submitInfo,
                VK_NULL_HANDLE);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't submit graphics queue (%i)", res);
        return false;
    }

    VkPresentInfoKHR presentInfo =
    {
        VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        nullptr,
        1,
        &vulkanContext.RenderFinishedSemaphore,
        1,
        &vulkanContext.Swapchain,
        &imageIndex,
        nullptr
    };

    res = vkQueuePresentKHR(
                vulkanContext.GraphicsQueue,
                &presentInfo);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Error during queue present (%i)", res);
        return false;
    }
    return true;
}

void ApplicationCore::UpdateDeltaTime()
{
    auto currentTime =
            high_resolution_clock::now();
    DeltaTime = duration<float, seconds::period>(
                currentTime - LastUpdateTime).count();
    LastUpdateTime = currentTime;
}

void ApplicationCore::UpdateMVP()
{
    const Core::VulkanContextData &context =
            VulkanManager->VulkanContext();

    glm::mat4 mod = glm::rotate(
                glm::mat4(1.0f),
                glm::radians(90.0f * CameraYaw),
                glm::vec3(0.0f , 0.0f, 1.0f));

    glm::vec3 axis =
            glm::vec4(0.0f, 1.0f, 0.0f, 0.0f) * mod;
    mod = glm::rotate(
                mod,
                glm::radians(90.0f * CameraPitch),
                axis);

    glm::mat4 view = glm::lookAt(
                glm::vec3(
                    5.0f,
                    0.0f,
                    0.0f),
                glm::vec3(
                    0.0f,
                    0.0f,
                    0.0f),
                glm::vec3(
                    0.0f,
                    0.0f,
                    1.0f));

    static const float aspect =
            context.ImageExtent.width / (float)context.ImageExtent.height;
    glm::mat4 proj = glm::perspective(
                glm::radians(CurrentFOV),
                aspect,
                0.1f,
                50.0f);
    proj[1][1] *= -1;

    glm::mat4 constants[2] =
    {
        mod,
        proj * view
    };

    VkCommandBuffer cmd =
            VulkanManager->BeginSingleTimeCommands();

    vkCmdPushConstants(
                cmd,
                context.PipelineLayout,
                VK_SHADER_STAGE_VERTEX_BIT,
                0,
                (uint32_t)sizeof(constants),
                &constants);

    VulkanManager->EndSingleTimeCommands(cmd);
}

void ApplicationCore::UpdateCamera()
{
    const Core::SDLInput input = SDLManager->CurrentInput();

    if(input.MouseR)
    {
        TargetFOV = ClampF(
                    TargetFOV
                    + input.MouseY * DeltaTime * 25.0f,
                    25.0f,
                    100.0f);
    }
    CurrentFOV = LerpF(
                CurrentFOV,
                TargetFOV,
                DeltaTime * 5.0f);

    if(input.MouseL)
    {
        CameraYawSpeed = ClampF(
                    CameraYawSpeed
                    + input.MouseX * DeltaTime * 25.0f,
                    -50.0f,
                     50.0f);
        CameraPitchSpeed = ClampF(
                    CameraPitchSpeed
                    + input.MouseY * DeltaTime * 25.0f,
                    -50.0f,
                    50.0f);
    }
    CameraYaw +=
            CameraYawSpeed * 0.125f * DeltaTime;
    CameraYawSpeed = LerpF(
                CameraYawSpeed,
                0.5f,
                DeltaTime * 3.0f);
    CameraPitch = ClampF(
                CameraPitch + CameraPitchSpeed * 0.25f * DeltaTime,
                -1.0f,
                1.0f);
    CameraPitchSpeed = LerpF(
                CameraPitchSpeed,
                0.0f,
                DeltaTime * 3.0f);
}

void ApplicationCore::GenerateVertexBuffer()
{
    const Core::SDLInput input =
            SDLManager->CurrentInput();

    float scroll = input.Shift ? 0.0005f : 0.0125f;
    CurrentThreshold = ClampF(
                CurrentThreshold + input.MouseW
                * scroll * (ThresholdMax - ThresholdMin),
                ThresholdMin,
                ThresholdMax);

    if(fabs(LastThreshold - CurrentThreshold) < 0.001f)
    {
        return;
    }

    LastThreshold = CurrentThreshold;

    const Core::VulkanContextData &context =
            VulkanManager->VulkanContext();

    VkCommandBuffer cmd =
            VulkanManager->BeginSingleTimeCommands();

    vkCmdBindPipeline(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                context.ComputePipeline);

    vkCmdBindDescriptorSets(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                context.ComputePipelineLayout,
                0,
                1,
                &context.DescriptorSet,
                0,
                0);

    vkCmdPushConstants(
                cmd,
                context.ComputePipelineLayout,
                VK_SHADER_STAGE_COMPUTE_BIT,
                0,
                (uint32_t)sizeof(CurrentThreshold),
                &CurrentThreshold);

    vkCmdDispatch(
                cmd,
                context.ComputeWorkgroupCount[0],
                context.ComputeWorkgroupCount[1],
                context.ComputeWorkgroupCount[2]);

    vkEndCommandBuffer(cmd);

    VkSubmitInfo submitInfo =
    {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        nullptr,
        0,
        VK_NULL_HANDLE,
        VK_NULL_HANDLE,
        1,
        &cmd,
        0,
        VK_NULL_HANDLE
    };
    vkQueueSubmit(
                context.ComputeQueue,
                1,
                &submitInfo,
                VK_NULL_HANDLE);
    vkQueueWaitIdle(context.ComputeQueue);
    vkFreeCommandBuffers(
                context.LogicalDevice,
                context.CommandPool,
                1,
                &cmd);
}

bool ApplicationCore::ReadInputData(
        const Main::InputOptions Options)
{
    switch(Options.mode)
    {
    default:
    case 0:
        CreateSphere();
        break;
    case 1:
        CreateCayleySurface();
        break;
    case 2:
        if(Options.integer)
        {
            switch(Options.width)
            {
            case 8:
                if(Options.sign)
                {
                    ReadFromFile<int8_t>(Options);
                }
                else
                {
                    ReadFromFile<uint8_t>(Options);
                }
                break;
            case 16:
                if(Options.sign)
                {
                    ReadFromFile<int16_t>(Options);
                }
                else
                {
                    ReadFromFile<uint16_t>(Options);
                }
                break;
            case 32:
                if(Options.sign)
                {
                    ReadFromFile<int32_t>(Options);
                }
                else
                {
                    ReadFromFile<uint32_t>(Options);
                }
                break;
            case 64:
                if(Options.sign)
                {
                    ReadFromFile<int64_t>(Options);
                }
                else
                {
                    ReadFromFile<uint64_t>(Options);
                }
                break;
            }
        }
        else
        {
            switch(Options.width)
            {
            case 16:
                ReadFromFile<short>(Options);
                break;
            case 32:
                ReadFromFile<float>(Options);
                break;
            case 64:
                ReadFromFile<double>(Options);
                break;
            }
        }
        break;
    }

    return true;
}

void ApplicationCore::CreateSphere()
{
    InputData = std::vector<float>(
                Dimensions[0]
                * Dimensions[1]
                * Dimensions[2],
                0.0f);

    for (int z = 2; z < Dimensions[2] - 2; ++z)
    {
        for (int y = 2; y < Dimensions[1] - 2; ++y)
        {
            for (int x = 2; x < Dimensions[0] - 2; ++x)
            {
                glm::vec3 size
                {
                    Dimensions[0],
                    Dimensions[1],
                    Dimensions[2]
                };
                glm::vec3 current
                {
                    x,
                    y,
                    z
                };
                float val =
                        1.0f - glm::length(0.5f * size - current)
                        / glm::length(size);

                InputData[(z * Dimensions[0] * Dimensions[1])
                        + (y * Dimensions[0])
                        + x] = val;

                if(val > ThresholdMax)
                {
                    ThresholdMax = val;
                }
                else if(val < ThresholdMin)
                {
                    ThresholdMin = val;
                }
            }
        }
    }
}

void ApplicationCore::CreateCayleySurface()
{
    InputData = std::vector<float>(
                Dimensions[0]
                * Dimensions[1]
                * Dimensions[2]);

    const float _scale =
            2.5f
            / std::min(
                std::min(
                    Dimensions[0],
                    Dimensions[1]),
                Dimensions[2]);
    for (int z = 0; z < Dimensions[2]; ++z)
    {
        for (int y = 0; y < Dimensions[1]; ++y)
        {
            for (int x = 0; x < Dimensions[0]; ++x)
            {
                const float _x =
                        _scale * (x - (Dimensions[0] / 2));
                const float _y =
                        _scale * (y - (Dimensions[1] / 2));
                const float _z =
                        _scale * (z - (Dimensions[2] / 2));

                const float val = 1.0f - 16.0f * _x * _y * _z
                        - 4.0f * (_x * _x)
                        - 4.0f * (_y * _y)
                        - 4.0f * (_z * _z);

                InputData[(z * Dimensions[0] * Dimensions[1])
                        + (y * Dimensions[0])
                        + x] = val;

                if(val > ThresholdMax)
                {
                    ThresholdMax = val;
                }
                else if(val < ThresholdMin)
                {
                    ThresholdMin = val;
                }
            }
        }
    }
}

template <typename T>
void ApplicationCore::ReadFromFile(
        const InputOptions Options)
{
    InputData.resize(
                Options.dim[0]
                * Options.dim[1]
                * Options.dim[2]);

    SDL_RWops *io = SDL_RWFromFile(
                Options.filePath.c_str(),
                "rb");

    size_t size = SDL_RWsize(io);
    size_t nb_read_total = 0, nb_read = 1;
    float *ptr = InputData.data();

    while(nb_read_total < size)
    {
        char raw[sizeof(T)];
        nb_read = SDL_RWread(
                    io,
                    &raw,
                    sizeof(T),
                    1);
        if(nb_read == 0)
        {
            break;
        }
        nb_read_total += nb_read;

        if(Options.flip)
        {
            int i, j;
            for(i = 0, j=sizeof(T) - 1; i < j; ++i, --j)
            {
                char tmp = raw[i];
                raw[i] = raw[j];
                raw[j] = tmp;
            }
        }

        T rawVal;
        memcpy(&rawVal, raw, sizeof(T));

        float val = (float)rawVal;
        *ptr = val;
        ptr++;

        if(val > ThresholdMax)
        {
            ThresholdMax = val;
        }
        else if(val < ThresholdMin)
        {
            ThresholdMin = val;
        }
    }
    SDL_RWclose(io);
}
