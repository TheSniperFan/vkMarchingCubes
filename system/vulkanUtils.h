#pragma once

#include <string>
#include <vulkan/vulkan.h>
#include "coreTypes.h"

namespace Core
{
    /**
     * @brief The VulkanManager class handles the initialization and cleanup of
     * the vulkan related data structures.
     */
    class VulkanManager
    {
    public:
        VulkanManager(
                const SDLStateInfo &SDLState,
                bool &OutSuccess,
                int &OutErrorCode,
                std::string &OutErrorString,
                const int Dimensions[3],
                const std::vector<float> &ScalarField);
        ~VulkanManager();

        VulkanContextData &VulkanContext()
        {
            return m_VulkanContext;
        }

        VkCommandBuffer BeginSingleTimeCommands();
        void EndSingleTimeCommands(
                VkCommandBuffer CommandBuffer);

    private:
        VulkanContextData m_VulkanContext;

        /**
         * @brief CreateInstance Creates a vulkan instance with the platform
         * specific extensions.
         * @param SDLState The SDL information used for creating the window.
         * @return Success.
         */
        bool CreateInstance(const SDLStateInfo &SDLState);
        /**
         * @brief CreateSurface Creates a vulkan surface with with the platform
         * specific extensions.
         * @param SDLState The SDL information used for creating the window.
         * @return Success.
         */
        bool CreateSurface(const SDLStateInfo &SDLState);
        bool CreateDevice();
        void CreateQueues();
        bool CreateSwapchain(const SDLStateInfo &SDLState);

        bool SupportsPresentMode(
                std::vector<VkPresentModeKHR> &InSupportedModes,
                VkPresentModeKHR Mode);
        void SetImageFormatAndColorSpace(
                std::vector<VkSurfaceFormatKHR> &InSupportedFormats,
                VkFormat &OutFormat,
                VkColorSpaceKHR &OutColorSpace);

        bool CreateSwapchainImageViews();
        bool CreateRenderPass();
        bool CreateDescriptorSetLayout();
        bool CreateGraphicsPipeline();
        bool LoadShaderFromDisk(
                std::vector<char> &OutShader,
                const char *Path);
        bool CreateShaderModule(
                const std::vector<char> &InShader,
                VkShaderModule &OutModule);
        bool CreateFramebuffers();
        bool CreateCommandPool();
        bool CreateDepthResources();
        bool CreateVertexBuffer();
        bool CreateComputeBuffers(
                const std::vector<float> &ScalarField);
        bool CreateDescriptorPool();
        bool CreateDescriptorSet();
        bool CreateCommandBuffers();
        bool CreateSemaphores();

        bool CreateComputePipeline();

        bool FindMemoryType(
                uint32_t TypeFilter,
                VkMemoryPropertyFlags Properties,
                uint32_t &OutType);
        bool FindSupportedFormat(
                const std::vector<VkFormat> &Canidates,
                VkImageTiling Tiling,
                VkFormatFeatureFlags Features,
                VkFormat &OutFormat);
        bool FindSupportedDepthFormat(
                VkFormat &OutFormat);
        bool HasStencilComponent(VkFormat Format);

        bool CreateBuffer(
                VkDeviceSize Size,
                VkBufferUsageFlags Usage,
                VkMemoryPropertyFlags Properties,
                VkBuffer &Buffer,
                VkDeviceMemory &Memory);
        void CopyBuffer(
                VkBuffer SrcBuffer,
                VkBuffer DstBuffer,
                VkDeviceSize Size);
        bool CreateImage(
                uint32_t Width,
                uint32_t Height,
                VkFormat Format,
                VkImageTiling Tiling,
                VkImageUsageFlags Usage,
                VkMemoryPropertyFlags Properties,
                VkImage &Image,
                VkDeviceMemory &ImageMemory);
        bool CreateImageView(VkImage Image,
                VkFormat Format,
                VkImageAspectFlags AspectFlags,
                VkImageView &OutView);
        bool TransitionImageLayout(
                VkImage Image,
                VkFormat Format,
                VkImageLayout OldLayout,
                VkImageLayout NewLayout);
        bool CopyDataToDevice(
                VkDeviceSize BufferSize,
                void *SourceData,
                VkBufferUsageFlags Usage,
                VkBuffer &DeviceBuffer,
                VkDeviceMemory &DeviceMemory);
    };
}
