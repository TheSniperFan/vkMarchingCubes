#include "sdlUtils.h"
#include "globalSettings.h"

#include <SDL_vulkan.h>


Core::SDLManager::SDLManager(
        bool &OutSuccess,
        int &OutErrorCode,
        std::string &OutErrorString)
{
    m_ShouldQuit = SDL_FALSE;
    SDL_memset(&m_SDLState, 0, sizeof(m_SDLState));
    OutErrorCode = SDL_InitSubSystem(SDL_INIT_VIDEO);
    if(OutErrorCode != 0)
    {
        OutSuccess = false;
        OutErrorString = std::string("SDL: ") + SDL_GetError();
        return;
    }

    int display;
    if(!ChooseDisplay(display))
    {
        OutSuccess = false;
        OutErrorString =
                std::string("SDLManager: ChooseDisplay() failed with: ")
                + SDL_GetError();
        OutErrorCode = 1;
        return;
    }

    if(!CreateWindow(display))
    {
        OutSuccess = false;
        OutErrorString =
                std::string("SDLManager: CreateWindow() failed with: ")
                + SDL_GetError();
        OutErrorCode = 1;
        return;
    }

    OutSuccess = true;
    OutErrorCode = 0;
    OutErrorString = "SUCCESS";
}

Core::SDLManager::~SDLManager()
{
    if(m_SDLState.Window)
    {
        SDL_DestroyWindow(m_SDLState.Window);
    }
    if(SDL_WasInit(SDL_INIT_VIDEO) != 0)
    {
        SDL_QuitSubSystem(SDL_INIT_VIDEO);
    }
}

SDL_bool Core::SDLManager::ChooseDisplay(int &displayIndex)
{
    int displayCount = SDL_GetNumVideoDisplays();
    if(displayCount < 1)
    {
        displayIndex = -1;
        SDL_SetError("No video displays found!");
        return SDL_FALSE;
    }

    if(displayCount == 1)
    {
        displayIndex = 0;
        return SDL_TRUE;
    }


    SDL_LogInfo(
                SDL_LOG_CATEGORY_VIDEO,
                "Found %i displays.",
                displayCount);

    SDL_Window *win;
    win = SDL_CreateWindow(
                "Display Selection",
                SDL_WINDOWPOS_CENTERED,
                SDL_WINDOWPOS_CENTERED,
                400,
                0,
                SDL_WINDOW_SHOWN);

    if(!win)
    {
        displayIndex = -1;
        return SDL_FALSE;
    }

    SDL_Event e;
    bool quit = false;
    while(!quit)
    {
        while(SDL_PollEvent(&e))
        {
            if(e.type == SDL_QUIT
               || (e.type == SDL_KEYDOWN
               && e.key.keysym.sym == SDLK_ESCAPE))
            {
                SDL_DestroyWindow(win);
                SDL_SetError("Cancelled by user.");
                return SDL_FALSE;
            }
            if(e.type == SDL_KEYDOWN
               && e.key.keysym.sym != SDLK_ESCAPE)
            {
                quit = true;
            }

            displayIndex = SDL_GetWindowDisplayIndex(win);
            SDL_SetWindowTitle(win, SDL_GetDisplayName(displayIndex));
        }
    }
    SDL_LogInfo(
                SDL_LOG_CATEGORY_VIDEO,
                "Selected display \"%s\"",
                SDL_GetDisplayName(displayIndex));
    SDL_DestroyWindow(win);
    return SDL_TRUE;
}

SDL_bool Core::SDLManager::CreateWindow(int display)
{
    SDL_Rect rect;
    SDL_GetDisplayBounds(display, &rect);
    m_SDLState.Window_h = rect.h;
    m_SDLState.Window_w = rect.w;
    m_SDLState.Window_x = rect.x;
    m_SDLState.Window_y = rect.y;

    m_SDLState.WindowFlags =
            SDL_WINDOW_VULKAN
            | SDL_WINDOW_BORDERLESS
            | SDL_WINDOW_MAXIMIZED
            | SDL_WINDOW_ALWAYS_ON_TOP;
    m_SDLState.WindowTitle = APPLICATION_NAME;

    m_SDLState.Window = SDL_CreateWindow(
                m_SDLState.WindowTitle,
                m_SDLState.Window_x,
                m_SDLState.Window_y,
                m_SDLState.Window_w,
                m_SDLState.Window_h,
                m_SDLState.WindowFlags);

    if(!m_SDLState.Window)
    {
        return SDL_FALSE;
    }

    SDL_GetWindowSize(
                m_SDLState.Window,
                &m_SDLState.Window_w,
                &m_SDLState.Window_w);
    return SDL_TRUE;
}

void Core::SDLManager::Update()
{
    m_CurrentInput.MouseW =
            m_CurrentInput.MouseX =
            m_CurrentInput.MouseY = 0.0f;


    SDL_Event e;
    while(SDL_PollEvent(&e))
    {
        if(e.type == SDL_QUIT)
        {
            m_ShouldQuit = SDL_TRUE;
        }
        else if(e.type == SDL_KEYDOWN)
        {
            switch (e.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                m_CurrentInput.Escape = SDL_TRUE;
                break;
            case SDLK_LSHIFT:
                m_CurrentInput.Shift = SDL_TRUE;
            default:
                break;
            }
        }
        else if(e.type == SDL_KEYUP)
        {
            switch (e.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                m_CurrentInput.Escape = SDL_FALSE;
                break;
            case SDLK_LSHIFT:
                m_CurrentInput.Shift = SDL_FALSE;
            default:
                break;
            }
        }
        else if(e.type == SDL_MOUSEBUTTONDOWN)
        {
            switch(e.button.button)
            {
            case SDL_BUTTON_LEFT:
                m_CurrentInput.MouseL = SDL_TRUE;
                SDL_SetRelativeMouseMode(SDL_TRUE);
                break;
            case SDL_BUTTON_RIGHT:
                m_CurrentInput.MouseR = SDL_TRUE;
                SDL_SetRelativeMouseMode(SDL_TRUE);
                break;
            }
        }
        else if(e.type == SDL_MOUSEBUTTONUP)
        {
            switch(e.button.button)
            {
            case SDL_BUTTON_LEFT:
                m_CurrentInput.MouseL = SDL_FALSE;
                SDL_SetRelativeMouseMode(SDL_FALSE);
                break;
            case SDL_BUTTON_RIGHT:
                m_CurrentInput.MouseR = SDL_FALSE;
                SDL_SetRelativeMouseMode(SDL_FALSE);
                break;
            }
        }
        else if(e.type == SDL_MOUSEWHEEL)
        {
            m_CurrentInput.MouseW = (float)e.wheel.y;
        }
        else if(e.type == SDL_MOUSEMOTION)
        {
            m_CurrentInput.MouseX = (float)e.motion.xrel;
            m_CurrentInput.MouseY = (float)e.motion.yrel;
        }
    }
}
