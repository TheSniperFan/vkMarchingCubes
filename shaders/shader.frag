#version 450

layout(location = 0) out vec3 outColor;
layout(location = 0) in vec3 vNorm;
layout(location = 1) in vec3 vPos;

const vec3 lightPosition = vec3(-2.0f, -5.0f, -2.0f);

vec3 BlinnPhong(
        vec3 ambient,
        vec3 diffuse,
        vec3 specular);

void main()
{
    outColor = BlinnPhong(
                vec3(0.15f, 0.0f, 0.1f),
                vec3(0.8f, 0.0f, 0.1f),
                vec3(1.0f, 1.0f, 1.0f));
}

vec3 BlinnPhong(
        vec3 ambient,
        vec3 diffuse,
        vec3 specular)
{
    vec3 lightDir = normalize(lightPosition - vPos);

    float spec = 0.0f;
    float lambert = max(dot(lightDir, vNorm), 0.0f);
    if(lambert > 0.0f)
    {
      vec3 viewDir = normalize(-vPos);
      vec3 halfDir = normalize(lightDir + viewDir);
      float specAngle = max(dot(halfDir, vNorm), 0.0f);
      spec = pow(specAngle, 30.0f);
    }

    return ambient +
            lambert * diffuse +
            spec * specular;
}
